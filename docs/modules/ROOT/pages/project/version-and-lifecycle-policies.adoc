= Antora Version and Lifecycle Policies
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
:keywords: EOL, GA
// Settings
:idprefix:
:idseparator: -
:table-caption!:
:core-version: 1.0
:ga-date: 2018/03/31
:gs-date: 2018/03/31
:eol-date: 2019/03/31
:eogs-date: 2019/03/31

== Version policy

Antora follows the semantic versioning rules.
Each Antora component release is versioned *major.minor.patch*.

Major::
Major releases occur when there are substantial changes in functionality or when new functionality breaks backwards compatibility.
Releases within the same major version number will maintain API compatibility.

Minor::
Minor releases add new features, improvements to existing features, and fixes that maintain backwards compatibility.

Patch::
Patch releases fix bugs and maintain backwards compatibility.
Only the latest minor release for a major release will receive patches.
Patch releases happen as needed depending on the urgency of the fix.

Pre-release::
Major and minor releases may include pre-release versions (major.minor.patch-alpha.n | -beta.n | -rc.n).
Pre-release versions will be tagged as _next_ so that the npm client doesn't prefer a pre-release over a stable version.
Once a release candidate (rc) has been thoroughly tested, the stable release will be published.

== Lifecycle policy

Each major version of the Antora Core components is supported for at least one year after the major version enters General Availability (GA).
Once a major version reaches End of Life (EOL), it will not receive maintenance releases.

[#lifecycle-phases]
=== Lifecycle phases

General Availability (GA)::
Software enters General Availability on the date an initial major version is released and is available for download.

General Support (GS)::
General Support begins on the date a major release enters General Availability.

End of General Support (EOGS)::
General Support ends on the date a major release reaches its End of Life.

End of Life (EOL)::
The date after which the software no longer receives maintenance releases.

[#ga-and-eol]
=== Release GA and EOL matrix

[cols="2,1,1,1,1"]
|===
|Antora Core Components	|GA |GS	|EOL |EOGS

|AsciiDoc Loader {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|CLI {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Content Aggregator {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Content Classifier {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Document Converter {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Navigation Builder {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Page Composer {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Playbook Builder {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Redirect Producer {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Site Generator Default {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Site Mapper {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|Site Publisher {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|UI Loader {core-version}
|{ga-date}
|{gs-date}
|{eol-date}
|{eogs-date}

|===

//{asterisk} Date is estimated and subject to change.

The information on this page is believed to be accurate as of the date of publication, but updates and revisions may be posted periodically and without notice.
