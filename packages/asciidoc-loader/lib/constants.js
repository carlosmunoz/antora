'use strict'

module.exports = Object.freeze({
  EXAMPLES_DIR_PROXY: 'example$',
  PARTIALS_DIR_PROXY: 'partial$',
})
